package com.springapp.mvc.service;

import com.springapp.mvc.model.User;

import java.util.List;

/**
 * Created by adri4n on 29.12.2015.
 */
public interface UserService {

    User findById(long id);

    void addUser(User user);

    void deleteUser(long id);

    void updateUser(User user);

    List<User> getAllUsers();

    boolean isUserExist(User user);
}
