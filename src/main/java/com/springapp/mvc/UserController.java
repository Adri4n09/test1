package com.springapp.mvc;

import com.springapp.mvc.model.User;
import com.springapp.mvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by adri4n on 29.12.2015.
 */
@RestController
@RequestMapping("/user/")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/users"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<User> listUsers() {
        List<User> users = userService.getAllUsers();
        return users;
    }

    @RequestMapping(value = {"/add"}, method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody boolean addUser(@RequestBody User user) {
        try {
            userService.addUser(user);
            System.out.println("adding user" + user);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
